#[allow(unused_doc_comments, unused_variables, dead_code, unused_mut)]
fn main() {
    println!("\n\n------------------------------------------------");
    ///: - Regular if else
    let some_bool: bool = true;
    let some_int: i32 = 30;
    let some_int2: i32 = 30;

    if some_bool == true || (some_int > 100 && some_int2 == 200) {
        println!("1. Hit if branch")
        //__________
    } else if some_int == 30 {
        println!("1. Hit else if branch")
        //__________
    } else { println!("1. Hit else branch") }

    /*------------------------------------------------*/
    ///: - Inline if else
    let var_from_inline: i32 = if some_int == 9 {
        300
    } else if some_int2 == -3 {
        0
    } else { 400 };

    println!("2. var_from_inline: {}", var_from_inline);

    /*------------------------------------------------*/
    ///: - Match
    match some_int {
        0 => println!("3. Hit 0 branch"),
        1..=100 => {
            println!("3. Between 1 and 100 branch");
            println!("   With braces...")
        },
        _ => println!("3. Hit else/default branch"),
    }

    let var_from_match: i32 = match some_bool { true => 10, false => 20 };
    println!("4. var_from_match: {}", var_from_match);

    /*------------------------------------------------*/
    ///: - Match with or
    let var_from_match2: i32 = match some_int {
        0 => 0,
        1 | 2 => 100,
        _ => {
            println!("5. Default branch");
            200
        },
    };

    println!("   var_from_match2: {}", var_from_match2);


    println!("------------------------------------------------");
}

