use simple_types::{print_difference, print_array, ding, on_off, print_distance};

#[allow(unused_doc_comments, unused_variables)]
fn main()
{
    println!("\n\n/*------------------------------------------------*/");

    ///: - Tuple destructuring
    let coords: (f32, f32) = (6.3, 15.0);
    print_difference(coords.0, coords.1);

    /*------------------------------------------------*/
    ///: - Array [type; size]
    let coords_array: [f32; 2] = [coords.0, coords.1];
    print_array(coords_array);

    let series: [i32; 7] = [1, 1, 2, 3, 5, 8, 13];
    ding(series[6]);

    /*------------------------------------------------*/
    ///: - Messy tuple
    let mess: ([i32; 2], f64, [(bool, i32); 2], i32, &str) = (
        [3, 2],
        3.14,
        [(false, -3),
            (true, -100)],
        5, "candy"
    );

    ///: - mess.2 = [(false, -3), (true, -100)]
    ///  - mess.2[1] = (true, -100)
    ///  - mess.2[1].0 = true
    on_off(mess.2[1].0);

    /*------------------------------------------------*/
    let distance: (f32, f32) = (20.3, 15.0);
    print_distance((distance.0, distance.1));
}



