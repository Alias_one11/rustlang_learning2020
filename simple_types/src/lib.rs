// lib.rs

pub fn print_difference(x: f32, y: f32) {
    println!("1. Difference between {} & {} is {}",
             x, y, (x - y).abs())
}

pub fn print_array(args: [f32; 2]) {
    println!("2. The coordinates are: ({}, {})", args[0], args[1])
}

pub fn ding(x: i32) {
    if x == 13 {
        println!("3. Ding! You found 13...")
    }
}

pub fn on_off(val: bool) {
    if val { println!("4. Lights are on!") }
}

pub fn print_distance(z: (f32, f32)) {
    println!("5. Distance to the origin is {:.2} meters",
             z.0.powf(2.0) + z.1.powf(2.0).sqrt())
}


