mod lib1;

use crate::SomeEnum::{OptionA, OptionB, OptionC};
use crate::lib1::{Point, PointGen, SomeEnum, alias_function};
use std::fmt::Debug;

#[allow(unused_doc_comments, unused_variables, dead_code, unused_mut)]
fn main()
{
    println!("\n\n------------------------------------------------");
    //__________
    //Regular struct
    let a: Point = Point { x: 100, y: -1 };
    println!("1. Point regular struct x: {}, y: {}", a.x, a.y);

    /*------------------------------------------------*/
    ///: - Generic type with struct
    let a_gen1: PointGen<i32, &str> = PointGen { x_gen: 100, y_gen: "&str-1" };
    println!("2. PointGen<i32, &str>(generic) x: {}, y: {}",
             a_gen1.x_gen, a_gen1.y_gen);

    let a_gen2: PointGen<f64, i32> = PointGen { x_gen: 100.09, y_gen: -1 };
    println!("3. PointGen<f64, i32>(generic) x: {}, y: {}",
             a_gen2.x_gen, a_gen2.y_gen);

    /*------------------------------------------------*/
    ///: - Generic type enumeration
    let some_data: SomeEnum<f64> = OptionA(34.2);

    match some_data {
        OptionA(a) => println!("4. Option A contains: {}", a),
        OptionB(a) => println!("4. Option B contains: {}", a),
        OptionC => println!("4. Option C contains: Default")
    }

    /*------------------------------------------------*/
    ///: - Generic type function
    alias_function(4 as i8, 5, "E-Type");

    /*------------------------------------------------*/
    ///: - Generic type trait with helper function & implementation
    let test = AliasStruct { some_int: 1700 };
    let result_of_test = do_this(&test);

    println!("------------------------------------------------");
}
/*------------------------------------------------*/

///: - Generic type trait
#[allow(dead_code, unused_variables)]
pub trait SomeCustomTrait {
    //__________
    fn what_eva(&self, arg1: &str, arg2: &str) -> String;
}

pub fn do_this<T>(arg: &T) -> String where T:SomeCustomTrait + Debug {
    //__________
    println!("6a. arg: &T: {:?}", arg);

    let arg_result = arg.what_eva("\n6c. arg1: &str", "\n6d. arg2: &str");
    println!("{}", arg_result);

    arg_result
}

#[allow(dead_code, unused_variables)]
#[derive(Debug)]
pub struct AliasStruct {
    some_int: i32
}

impl SomeCustomTrait for AliasStruct {
    //__________
    fn what_eva(&self, arg1: &str, arg2: &str) -> String {
        //__________
        "6b. what_eva(): ".to_owned() + &self.some_int.to_string() + arg1  + arg2
    }
}
