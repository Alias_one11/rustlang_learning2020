
#[allow(unused_doc_comments, unused_variables, dead_code, unused_mut)]
pub trait SomeTrait {
    //: - Functions inside a trait do not
    //  - have to be specified as public or not public
    fn is_valid(&self) -> bool;
}

#[allow(unused_doc_comments, unused_variables, dead_code, unused_mut)]
pub struct RandomInfo {
    pub call_count: i64,
    pub some_bool: bool,
    pub some_int: i64,
}

impl SomeTrait for RandomInfo {
    //__________
    fn is_valid(&self) -> bool {
        self.some_bool
    }
}

#[allow(unused_doc_comments, unused_variables, dead_code, unused_mut)]
///: - Associated functions: NOTE:: Works like `extension` in swift on
///  - a struct which extends its functionality further
impl RandomInfo {
    //__________
    pub fn new_info(arg_a: bool) -> Self {
        Self {
            call_count: 0,
            some_bool: !arg_a,
            some_int: 17,
        }
    }

    #[allow(clippy::wrong_self_convention)]
    pub fn is_smaller(&mut self, compare_to: i64) -> bool {
        //__________
        //: - Increments by one on call of self or the object/singleton..etc
        self.call_count += 1;
        self.some_int < compare_to
    }
}


