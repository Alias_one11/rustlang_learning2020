use enumerations::{process_payment, Payment, DebitData};
use Payment::{Cash, DebitCard, Crypto, CreditCard};

#[allow(unused_doc_comments, unused_variables, dead_code, unused_mut)]
fn main() {
    println!("\n\n------------------------------------------------");
    //__________
    ///: - Cash
    let cash_payment: Payment = Cash(100.);
    process_payment(cash_payment);

    ///: - CreditCard
    let creditcard_payment: Payment = CreditCard(
        "Credit Card#: ···· ···· ····  1798".to_string(),
        250.
    );
    process_payment(creditcard_payment);

    ///: - DebitCard
    let debitcard_payment: Payment = DebitCard(DebitData {
        card_number: "Debit Card#: ···· ···· ····  3456".to_string(),
        amount: 400.
    });
    process_payment(debitcard_payment);

    ///: - Crypto
    let crypto_payment: Payment = Crypto {
        account_id: "136baa3e-3798-4363-b08a-f3854a01dc6d".to_string(),
        amount: 1000.
    };
    process_payment(crypto_payment);

    println!("------------------------------------------------");
}







