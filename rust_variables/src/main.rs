use rust_variables::hola;

///: - Globally available
const STARTING_MISSILES: i32 = 8;
const READY_AMOUNT: i32 = 2;

#[allow(unused_doc_comments, unused_variables)]
fn main()
{
    ///: - Modules
    hola();

    ///: - Properties
    let mut missiles = STARTING_MISSILES;
    let ready = READY_AMOUNT;

    println!("\n\n1. Firing {} of my {} missiles...",
             ready, missiles);

    missiles -= ready;
    println!("2. {} missiles left...", missiles);
}

