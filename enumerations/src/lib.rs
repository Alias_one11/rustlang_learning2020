pub enum Payment {
    Cash(f32),
    CreditCard(String, f32),
    DebitCard(DebitData),
    //: - Strongly typed enum item, similar to a struct
    Crypto { account_id: String, amount: f32 }
}

/*------------------------------------------------*/
pub struct DebitData {
    pub card_number: String,
    pub amount: f32,
}

/*------------------------------------------------*/
pub fn process_payment(some_payment_type: Payment) {
    //__________
    match some_payment_type {
        Payment::Cash(amount) => {
            println!("1. Paying with cash...\
             \nIn the amount of: ${:.2}", amount)
        }
        Payment::CreditCard(some_str, some_f32) => {
            println!("\n2. Paying with credit card...\
             \nsome_str: {}, some_f32: ${:.2}", some_str, some_f32)
        }
        Payment::DebitCard(data) => {
            println!("\n3. Paying with debit...\
             \ncard_number: {}, amount: ${:.2}",
                data.card_number, data.amount)
        }
        //: - Have to match the name in the `Crypto` definition
        Payment::Crypto { account_id, amount } => {
            println!("\n4. Paying with crypto... \
            \naccount_id: {}, amount: ${:.2}", account_id, amount)
        }
        //: - Escape clause so you do not have
        //  - to exhaust all types in the enum
        // _=> {}
    }
}



