//: - lib1

use std::fmt::Display;
use std::ops::{Add, Sub};

///: - Generic type struct
pub struct PointGen<T, U> {
    pub x_gen: T,
    pub y_gen: U,
}

///: - Generic type enumeration
#[allow(dead_code)]
pub enum SomeEnum<T> {
    OptionA(T),
    OptionB(T),
    OptionC,
}

#[allow(dead_code)]
pub struct Point {
    ///: - Points on a graph
    pub x: i32,
    pub y: i32,
}

///: - Generic type function
#[allow(dead_code, unused_variables)]
pub fn alias_function<T, E>(arg1: T, arg2: T, arg3: E) -> T where
    T: Display + Add<Output = T> + Sub<Output = T>,
    E: Display {
    //__________
    let result = arg1 + arg2;
    println!("5a. Generic some_func: {}", result);

    println!("5b. Generic some_func: {}", arg3);
    result
}