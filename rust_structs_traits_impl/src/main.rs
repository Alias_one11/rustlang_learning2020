mod random_info;

use random_info::*;

#[allow(unused_doc_comments, unused_variables, dead_code, unused_mut)]
fn main() {
    //__________
    println!("\n\n------------------------------------------------");
    let mut random_info_props = RandomInfo {
        call_count: 0,
        some_int: 5,
        some_bool: false,
    };

    println!("1a. some_int: {}\n1b. some_bool: {}",
        random_info_props.some_int, random_info_props.some_bool);

    let is_this_smaller: bool = random_info_props.is_smaller(9);
    println!("2. Is it smaller then some_int: {}", is_this_smaller);

    let is_this_larger: bool = random_info_props.is_larger(20);
    println!("3. Is it larger then some_int: {}", is_this_larger);

    //: - Using our trait decouples the trait to be used on multiple structs
    let is_valid: bool = random_info_props.is_valid();
    println!("4. Some trait in action-->is_valid: {}", is_valid);

    print_if_is_valid(&random_info_props);

    let random_info_props2 = AliasData::default();
    println!("6a. random_info_props2.some_bool: {}\
    \n6b. random_info_props2.some_int: {}",
        random_info_props2.some_bool, random_info_props2.some_int);
    /*------------------------------------------------*/
    ///: - Declaring a singleton
    let mut alias_props = AliasData {
        some_bool: true,
        some_float: 17.0,
        some_int: 80,
        random: RandomInfo::new_info(true),
    };

    alias_props.some_int = 90;

    ///: - Synthetic sugar 🍭
    let alias_props2 = AliasData {
        some_int: 70,
        ..alias_props
    };
    println!("------------------------------------------------");
}

/*------------------------------------------------*/
#[allow(dead_code)]
struct AliasData {
    //__________
    some_int: i32,
    some_float: f64,
    some_bool: bool,
    random: RandomInfo,
}

impl Default for AliasData {
    fn default() -> Self {
        Self {
            some_int: 0,
            some_float: 0.0,
            some_bool: false,
            random: RandomInfo::new_info(true),
        }
    }
}

impl RandomInfo {
    //__________
    pub fn is_larger(&self, compare_to: i64) -> bool {
        //__________
        self.some_int > compare_to
    }
}

impl SomeTrait for AliasData {
    //__________
    fn is_valid(&self) -> bool { true }
}

pub fn print_if_is_valid(check_me: &dyn SomeTrait) {
    if check_me.is_valid() { println!("5. All valid here!") }
}